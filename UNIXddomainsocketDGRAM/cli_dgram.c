/************************************************************/
/* This datagram socket client program for UNIX */
/* domain sockets. This program creates a socket and sends  */
/* data to a server.                                        */
/************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#undef	sock_errno
#undef	sock_errstr
#define sock_errno()	errno
#define sock_errstr(e)	STRERROR(e)

#define SERVER_PATH "sock.svr_cli"

#define SECONDS_PER_MINUTE 60 

int array_gen(char *, int);

/* create a buffer of random upper case letters */
int array_gen(char *buffer, int length) {

    int i = length - 1;
    int r = rand() % 26; 

    buffer[i] = '\0';

    while (0 != i) {
        i = i - 1;
        buffer[i] = 'A' + r;
        r++;
        if (r==26)
           r = 0;
    }
    return 0;
}
double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;

    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;

    diff = (double)y_ms - (double)x_ms;
    return diff;
}

int send_msgs(int mins, int msg_size, int sockno, char svr_count)   {

    int client_socket, rc;
    struct sockaddr_un remote;
    int i;
    int tot_msgs=0;
    time_t start = time(NULL);

    struct timeval tv_pre_send;
    struct timeval tv_post_send;

    double time_elapsed;
    double last_max_elapsed, max_elapsed;
    double last_min_elapsed, min_elapsed;
    double avg_elapsed, tot_elapsed;

    char the_buffer[msg_size];

    memset(&remote, 0, sizeof(struct sockaddr_un));

    array_gen(the_buffer, msg_size);

    /****************************************/
    /* Create a UNIX domain datagram socket */
    /****************************************/
    client_socket = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (client_socket == -1) {
        printf("SOCKET ERROR = %d\n", sock_errno());
        exit(1);
    }

    /***************************************/
    /* Set up the UNIX sockaddr structure  */
    /* by using AF_UNIX for the family and */
    /* giving it a filepath to send to.    */
    /***************************************/
    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, SERVER_PATH);

    /***************************************/
    /* Copy the data to be sent to the     */
    /* buffer and send it to the server.   */
    /***************************************/

    i=0;
    while (time(NULL) - start < (time_t) ( mins * SECONDS_PER_MINUTE))  {
        array_gen(the_buffer, msg_size);
	gettimeofday(&tv_pre_send, NULL);
        rc = sendto(client_socket, the_buffer, strlen(the_buffer), MSG_CONFIRM, (struct sockaddr *) &remote, sizeof(remote));
        if (rc == -1) {
            printf("SENDTO ERROR = %d\n", sock_errno());
            close(client_socket);
            exit(1);
        }
        else {
	    gettimeofday(&tv_post_send, NULL);
            tot_msgs++;
            array_gen(the_buffer, msg_size);
        }
        time_elapsed = time_diff(tv_pre_send, tv_post_send);
	tot_elapsed+=time_elapsed;
	if (time_elapsed > last_max_elapsed)  {
		max_elapsed = time_elapsed;
		last_max_elapsed=time_elapsed;
	}
	if (time_elapsed < last_min_elapsed)  {
		min_elapsed = time_elapsed;
		last_min_elapsed=time_elapsed;
	}
    }

    /*****************************/
    /* Close the socket and exit */
    /*****************************/
    rc = close(client_socket);
    
    avg_elapsed = tot_elapsed/tot_msgs;
    /*printf("tot elapsed= %.0f\n",  tot_elapsed); */

    printf("message size: %d    total messages sent: %d  process number: %d server count: %d   duration: %d min   min latency  %.0f avg latency %6.2f  max latency %.0f usec\n", msg_size, tot_msgs, sockno, svr_count, mins, min_elapsed, avg_elapsed, max_elapsed);

    return 0;

}

int main(int argc, char **argv)
{

    time_t start = time(NULL);
    int MINS=1;
    int MSG_SZ;
    int socket_no;
    char svr_count;
    int c;


    if (argc != 9)
    {
            printf("wrong number of arguments    arg count=%d \n", argc);
            printf("-s is message size in bytes, -m is minutes, -p is process number, -c server count\n");
            printf("    Like this:  ./cli_dgram -s 1024 -m 2 p 2 \n");
            exit(1);
    }
    while ((c = getopt(argc, argv, "s:m:p:c:" )) != -1) {

            switch (c) {
                    case 's':
                            MSG_SZ = atoi(optarg);
                            break;
                    case 'm':
                            MINS = atoi(optarg);
                            break;
                    case 'p':
                            socket_no = atoi(optarg);
                            break;
		    case 'c':
			    svr_count = atoi(optarg);
                            break;
                }
    }

    send_msgs(MINS, MSG_SZ, socket_no, svr_count);

    return 0;
}


