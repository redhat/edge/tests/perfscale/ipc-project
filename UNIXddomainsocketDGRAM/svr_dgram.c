/************************************************************/
/* This is a datagram socket server sample program for UNIX */
/* domain sockets. This program creates a socket and        */
/* receives data from a client.                             */
/************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#undef	sock_errno
#undef	sock_errstr
#define sock_errno()	errno
#define sock_errstr(e)	STRERROR(e)

#define SOCK_PATH "sock.svr_cli"

int recv_msgs(int msg_size) {

    int server_sock, len, rc;
    int bytes_rec = 0;
    struct sockaddr_un server_sockaddr, peer_sock;
    char buf[msg_size];
    char the_buffer[msg_size];

    memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));
    /*memset(buf, 0, MSG_SZ); */

    /****************************************/
    /* Create a UNIX domain datagram socket */
    /****************************************/
    server_sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (server_sock == -1){
        printf("SOCKET ERROR = %d", sock_errno());
        exit(1);
    }

    /***************************************/
    /* Set up the UNIX sockaddr structure  */
    /* by using AF_UNIX for the family and */
    /* giving it a filepath to bind to.    */
    /*                                     */
    /* Unlink the file so the bind will    */
    /* succeed, then bind to that file.    */
    /***************************************/
    server_sockaddr.sun_family = AF_UNIX;
    strcpy(server_sockaddr.sun_path, SOCK_PATH);
    len = sizeof(server_sockaddr);
    unlink(SOCK_PATH);
    rc = bind(server_sock, (struct sockaddr *) &server_sockaddr, len);
    if (rc == -1){
        printf("BIND ERROR = %d", sock_errno());
        close(server_sock);
        exit(1);
    }

    /****************************************/
    /* Read data on the server from clients */
    /* and print the data that was read.    */
    /****************************************/

    while (1 == 1)
    {
        bytes_rec = recvfrom(server_sock, buf, msg_size, MSG_CONFIRM, (struct sockaddr *) &peer_sock, &len);
        if (bytes_rec == -1){
            printf("RECVFROM ERROR = %d", sock_errno());
            close(server_sock);
            exit(1);
        }
        memcpy(the_buffer, buf, msg_size-1);

	if (msg_size != 1)
          if (the_buffer[msg_size-2] == 'A') {
              the_buffer[msg_size-2] = 'B';
          }

    }

    printf("Total messages received: \n");

    /*****************************/
    /* Close the socket and exit */
    /*****************************/
    close(server_sock);

    return 0;

}


int main(int argc, char **argv){

    int MSG_SZ;
    int c;

    if (argc != 3)
    {
            printf("wrong number of arguments    arg count=%d \n", argc);
            printf("-s is message size in bytes, -p is process number\n");
            printf("Like this:  ./svr_dgram -s 1024 \n");
            exit(1);
    }
    
    while ((c = getopt(argc, argv, "s:p:" )) != -1)

            switch (c) {
                    case 's':
                            MSG_SZ = atoi(optarg);
                            break;
        }


    recv_msgs(MSG_SZ);

    return 0;
}
