
/* Author:  Matt Currier -  Auto perf and scale group */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>

#undef  sock_errno
#define sock_errno()    errno

void error(char *msg) {
    perror(msg);
}

int recv_msgs(char *memfd, int msg_size) {

    int server_sock, len, rc;
    int bytes_rec = 0;
    struct sockaddr_un server_sockaddr, peer_sock;
    char buf[msg_size];
    char the_buffer[msg_size];
    int fd;

    fd = open(memfd, O_RDONLY);
    if (fd == -1)
        error("open()");

    memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));

    /****************************************/
    /* Create a UNIX domain datagram socket */
    /****************************************/
    server_sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (server_sock == -1){
        printf("SOCKET ERROR = %d", sock_errno());
        exit(1);
    }

    /***************************************/
    /* Set up the UNIX sockaddr structure  */
    /* by using AF_UNIX for the family and */
    /* giving it a filepath to bind to.    */
    /*                                     */
    /* Unlink the file so the bind will    */
    /* succeed, then bind to that file.    */
    /***************************************/
    
    server_sockaddr.sun_family = AF_UNIX;
    strcpy(server_sockaddr.sun_path, memfd);
    
    len = sizeof(server_sockaddr);
    unlink(memfd);

    rc = bind(server_sock, (struct sockaddr *) &server_sockaddr, len);
    if (rc == -1){
        printf("BIND ERROR = %d\n", sock_errno());
        close(server_sock);
        exit(1);
    }

    /****************************************/
    /* Read data on the server from clients */
    /* and print the data that was read.    */
    /****************************************/

    while (1 == 1)
    {
        bytes_rec = recvfrom(server_sock, buf, msg_size, MSG_CONFIRM, (struct sockaddr *) &peer_sock, &len);
        if (bytes_rec == -1){
            printf("RECVFROM ERROR = %d\n", sock_errno());
            close(server_sock);
            exit(1);
        }
	/* copy received buffer to new buffer and check a check/change a character in it */
        memcpy(the_buffer, buf, msg_size-1);

	if (msg_size != 1)
        if (the_buffer[msg_size-2] == 'A') {
             the_buffer[msg_size-2] = 'B';
        }
    }

    /*****************************/
    /* Close the socket and exit */
    /*****************************/
    close(server_sock);
    return 0;
}

int main(int argc, char *argv[]) {
    int ret;
    int fd;
    int len;

    len = atoi(argv[2]);
    char buf[len];

    /* communicate over the named socket pointing to memfd region */
    recv_msgs(argv[1], len);

    fsync(fd);

    if (close(fd) == -1)
        error("close()");

    exit(0);
}

