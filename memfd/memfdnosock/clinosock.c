#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

void error(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void error2(char *msg) {
    perror(msg);
}

int main(int argc, char *argv[]) {
    int ret;
    int fd;
    int len;
    long msgssent;
    int tot_msgs = 0;

    char msgnumchar[10];
    long msgnum=1;
    long msgnumsent;

    fd = open(argv[1], O_RDONLY,O_ASYNC);
    if (fd == -1)
        error2("open()");

    len = atoi(argv[2]);

    msgssent=atol(argv[3]);

    char buf[len];

    while (1 == 1) {
        ret =read(fd, buf, len);
        if (ret == -1) {
            error2("read()");
	    break;
        }
	else
		if (ret != len) {
			printf("wrong number of bytes read. %d\n",ret);
		        continue;/*	exit(1); */
		}
	        else  {
		    strncpy(msgnumchar, buf, 9);
		    msgnumsent=atol(msgnumchar);
		    tot_msgs++;
		    printf("%d\n", msgnumsent ); 
		}
	if (close(fd) == -1) {
            error2("close()");
            break;
    	}
	fd = open(argv[1], O_RDONLY, O_ASYNC);
        if (fd == -1)   {
            error2("open()");
            break;
        }
    }
    if (close(fd) == -1) {
            error2("close()");
    }
    
    printf("total messages read:  %d \n", tot_msgs);

    if (close(fd) == -1)
        error2("close()");

}

