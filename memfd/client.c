/* Author:  Matt Currier -  Auto perf and scale group */

#define _GNU_SOURCE

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#undef  sock_errno
#undef  sock_errstr
#define sock_errno()    errno
#define sock_errstr(e)  STRERROR(e)

#define SECONDS_PER_MINUTE 60

/* create a buffer of uppercase characters the size of the message size specified */

int msg_gen(char *, int);

/* create a buffer of random upper case letters */
int msg_gen(char *buffer, int length) {

    static int seq = 1;
    int i = length - 1;
    static int r = 0;
    
    if (r==25)
        r = 0;
    else
        r++;

    buffer[i] = '\0';
    sprintf(buffer, "%9d ", seq);
    
    if (seq == 1)  r=0;

    while (10 != i) {
        i = i - 1;
        buffer[i] = 'A' + r;
    }
    
    seq++;
    return 0;
}

int send_msgs(int fd, int mins, int msg_size, char *name)   {

    int client_socket, rc;
    struct sockaddr_un remote;
    int i;
    int ret;
    int tot_msgs=0;
    time_t start = time(NULL);

    char buf[msg_size];
    
    memset(&remote, 0, sizeof(struct sockaddr_un));

    /****************************************/
    /* Create a UNIX domain datagram socket */
    /****************************************/
    client_socket = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (client_socket == -1) {
        printf("SOCKET ERROR = %d\n", sock_errno());
        exit(1);
    }

    /***************************************/
    /* Set up the UNIX sockaddr structure  */
    /* by using AF_UNIX for the family and */
    /* giving it a filepath to send to.    */

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, name);

    /***************************************/
    /* Copy the data to be sent to the     */
    /* buffer and send it to the server.   */
    /***************************************/

    start = time(NULL);
    msg_gen(buf, msg_size);

    char *p = mmap(NULL, msg_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (p == MAP_FAILED)
          err(EXIT_FAILURE,"mmap");

    while (time(NULL) - start < (time_t) ( mins * SECONDS_PER_MINUTE)) {  
    /*while (tot_msgs < 1000000) {  */

         strcpy(p, buf); 
	 rc = sendto(client_socket, p, strlen(p), MSG_CONFIRM, (struct sockaddr *) &remote, sizeof(remote));
       	 if (rc == -1) {
            	printf("SENDTO ERROR = %d\n", sock_errno());
            	close(client_socket);
            	exit(1);
         }
       	 else {
            	tot_msgs++;
            	msg_gen(buf, msg_size);
        	}

        /*printf("%s\n", p); */
    }

    ret = munmap(p, msg_size);
    if (ret == -1)
        err(EXIT_FAILURE, "munmap");

    rc = close(client_socket);

    printf("message size: %d   total messages sent: %d  %d /min    duration: %d min\n", msg_size, tot_msgs, tot_msgs/60,  mins);

    return 0;
    }

int main(int argc, char *argv[])
{
           int           fd;
           char          *name, *seals_arg;
           ssize_t       len;
           unsigned int  seals;
	   time_t start;
	   int mins;
	   int tot_msgs=0;
           int ret;
	   char mfd[20];

           if (argc < 3) {
               fprintf(stderr, "%s name size mins [seals]\n", argv[0]);
               fprintf(stderr, "\t'seals' can contain any of the "
                       "following characters:\n");
               fprintf(stderr, "\t\tg - F_SEAL_GROW\n");
               fprintf(stderr, "\t\ts - F_SEAL_SHRINK\n");
               fprintf(stderr, "\t\tw - F_SEAL_WRITE\n");
               fprintf(stderr, "\t\tW - F_SEAL_FUTURE_WRITE\n");
               fprintf(stderr, "\t\tS - F_SEAL_SEAL\n");
               exit(EXIT_FAILURE);
           }

           name = argv[1];
           len = atoi(argv[2]);
	   mins = atoi(argv[3]);
           seals_arg = argv[4];

           /* Create an anonymous file in tmpfs; allow seals to be
              placed on the file. */

           fd = memfd_create(name, MFD_ALLOW_SEALING);
           if (fd == -1)
               err(EXIT_FAILURE, "memfd_create");

           /* Size the file as specified on the command line. */

           if (ftruncate(fd, len) == -1)
               err(EXIT_FAILURE, "truncate");

           printf("PID: %jd; fd: %d; /proc/%jd/fd/%d\n",
                  (intmax_t) getpid(), fd, (intmax_t) getpid(), fd);
	   sprintf(mfd,"/proc/%jd/fd/%d",(intmax_t) getpid(), fd, (intmax_t) getpid(), fd);

	   printf("mfd=%s    name=%s\n", mfd, name);

           /* If a 'seals' command-line argument was supplied, set some
              seals on the file. */

           if (seals_arg != NULL) {
               seals = 0;

               if (strchr(seals_arg, 'g') != NULL)
                   seals |= F_SEAL_GROW;
               if (strchr(seals_arg, 's') != NULL)
                   seals |= F_SEAL_SHRINK;
               if (strchr(seals_arg, 'w') != NULL)
                   seals |= F_SEAL_WRITE;
               if (strchr(seals_arg, 'W') != NULL)
                   seals |= F_SEAL_FUTURE_WRITE;
               if (strchr(seals_arg, 'S') != NULL)
                   seals |= F_SEAL_SEAL;

               if (fcntl(fd, F_ADD_SEALS, seals) == -1)
                   err(EXIT_FAILURE, "fcntl");
           }

	   start = time(NULL);
	   send_msgs(fd, mins, len, name);

           close(fd);
	   exit(EXIT_SUCCESS);
}
